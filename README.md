# Arbitrage

Bot d'arbitrage opérant sur la blockchain Tezos. Il analyse et arbitre entre deux DEX, QuipuSwap et SpicySwap

## Paires

Le bot arbitre sur les paires suivante:
- [ ] WTZ/SPI
- [ ] WTZ/hDAO
- [ ] WTZ/FLAME

# Liquidation

Bot de liquidation sur https://ctez.app/



